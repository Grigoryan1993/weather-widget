import { combineReducers } from 'redux';
import getReducer from './getReducer';
import modalReducer from './modalReducer';


const rootReducer = combineReducers({
   getReducer,
   modalReducer,
})

export default rootReducer;