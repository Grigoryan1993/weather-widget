
const initalState = [];

const getReducer = (state = initalState, action) => {

  switch (action.type) {
    case 'CITY_NAME_TEMP':
      return action.payload;
    case 'CITY_DATA_UP_OR_DOWN':
      return action.payload;
    default:
      return state;
  }
}

export default getReducer;