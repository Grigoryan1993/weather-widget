const initalState = [];

const modalReducer = (state = initalState, action) => {
  switch (action.type) {
    case 'DELET_REMOVE_MODAL':
      return action.show;
    case 'RENAME_CITY':
      return action.open;
    default:
      return state;
  }
}

export default modalReducer;