import React from 'react';


class Input extends React.Component {

  render() {
    return (
      <input type="text" value={this.props.value} onKeyDown={this.props.onKeyDown} onChange={this.props.onChange} placeholder={this.props.placheholder} />
    );
  }
}



export default Input;