import React from 'react';
import Button from '../button';
import Input from '../input';
import { connect } from 'react-redux'

const API_KEY = 'f6143786dd5faed04b83e85ff5aa690c';
class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = { value: '' }
        this.hendleSubmit = this.hendleSubmit.bind(this);
        this.hendleChange = this.hendleChange.bind(this);
        this.onKeyPress = this.onKeyPress.bind(this);

    }
    hendleChange(event) {
        this.setState({ value: event.target.value });
    }


    onKeyPress(event) {
        if (event.keyCode == 13) {

            return this.hendleSubmit
        }
    }

    hendleSubmit = async (event) => {
        event.preventDefault();
        const res = await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${this.state.value}&appid=${API_KEY}`)
        if (res.status === 200) {
            const data = await res.json();
            const { id } = data;
            const temp = (data.main.temp - 273.15).toFixed(1);
            const { name } = data;

            if (this.props.prev.find(item => item.id == data.id) === undefined) {
                this.props.dispatch({ type: 'CITY_NAME_TEMP', payload: [...this.props.prev, { name, temp, id, status: 'Active' }] });
            }

        } else if (this.state.value == '') {
            alert('Add the name of city')

        } else {
            alert('Сity ​​not found')
        }

    }


    render() {
        return (
            <form className='searchBar' onKeyDown={this.onKeyPress} >
                <Input value={this.state.value} onChange={this.hendleChange} placheholder='Enter City Name' />
                <Button name='Search' onClick={this.hendleSubmit} />
            </form>
        )
    }
}

const mapStateToProps = (state) => ({
    prev: state.getReducer
})

export default connect(mapStateToProps)(Form);