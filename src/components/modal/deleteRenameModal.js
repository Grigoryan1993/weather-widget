import React, { Component } from 'react'
import { Button, Modal } from 'react-bootstrap'
import { connect } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css';




class BootstrapModal extends Component {
  constructor(props) {
    super(props)
    this.handleClose = this.handleClose.bind(this);
    this.handleStatus = this.handleStatus.bind(this);
  }

  handleClose() {
    this.props.dispatch({ type: 'DELET_REMOVE_MODAL', show: [{ show: false }] })
  }

  handleStatus(itemId) {
    const newCities = this.props.cities.map(item => {
      const city = { ...item };
      if (itemId === item.id) {
        city.status = (item.status === 'Active') ? 'Deleted' : 'Active';
      }
      return city;
    });
    this.props.dispatch({ type: 'CITY_NAME_TEMP', payload: newCities })
    return this.handleClose();
  }

  render() {

    return (
      <>
        {
          <Modal show={this.props.show} className="modal">
            <Modal.Header className="modal-header">
              <Modal.Title>Hello</Modal.Title>
            </Modal.Header>

            <Modal.Body>

            </Modal.Body>

            <Modal.Footer>
              <Button onClick={this.handleClose}>Close</Button>
              <Button onClick={() => this.handleStatus(this.props.item)}>{(this.props.elemStatus == 'Active') ? 'Delete' : 'Remove'}</Button>
            </Modal.Footer>
          </Modal>
        }
      </>
    )
  }
}
const mapStateToProps = (state) => ({
  cities: state.getReducer,
  showClose: state.modalReducer,
})


export default connect(mapStateToProps)(BootstrapModal);
