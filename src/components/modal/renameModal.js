import React, { Component } from 'react'
import { Button, Modal } from 'react-bootstrap'
import { connect } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css';




class RenameModal extends Component {
  constructor(props) {
    super(props)
    this.handleClose = this.handleClose.bind(this);
    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleChangeTemp = this.handleChangeTemp.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.state = {
      nameItem: this.props.name,
      tempItem: this.props.temp,
    }
  }

  handleChangeName(event) {
    this.setState({
      nameItem: event.target.value,
    })
  }

  handleChangeTemp(event) {
    this.setState({
      tempItem: event.target.value,
    })
  }

  handleClose() {
    this.props.dispatch({ type: 'RENAME_CITY', open: [{ open: false }] })
  }

  handleSave(itemId) {
    const newCities = this.props.cities.map(item => {
      const city = { ...item };
      if (itemId === item.id) {
        city.name = this.state.nameItem;
        city.temp = this.state.tempItem
      }
      return city;
    });
    this.props.dispatch({ type: 'CITY_NAME_TEMP', payload: newCities })
    return this.handleClose();
  }

  render() {

    return (
      <>
        {

          <Modal show={this.props.show} className="modal">
            <Modal.Header className="modal-header">
              <Modal.Title>
                City ​​Temperature
          </Modal.Title>
            </Modal.Header>

            <Modal.Body>
              <div>
                <label for='name'> Name - </label>
                <input type='text' id='name' value={this.state.nameItem} onChange={this.handleChangeName} />

                <label for='temp'> Gradus - </label>
                <input type='number' id='temp' value={this.state.tempItem} onChange={this.handleChangeTemp} />
              </div>
            </Modal.Body>

            <Modal.Footer>
              <Button onClick={this.handleClose}>Close</Button>
              <Button onClick={() => this.handleSave(this.props.item)}
                disabled={(this.state.nameItem == '' || this.state.tempItem == '') ? true : false}>
                Save
          </Button>
            </Modal.Footer>
          </Modal>
        }
      </>
    )
  }
}
const mapStateToProps = (state) => ({
  cities: state.getReducer,
})


export default connect(mapStateToProps)(RenameModal);