import React from 'react';
import Button from '../button';
import { connect } from 'react-redux';
import BootstrapModal from '../modal/deleteRenameModal';
import RenameModal from '../modal/renameModal';

class CityData extends React.Component {
    constructor(props) {
        super(props);
        this.upOrDown = this.upOrDown.bind(this);
        this.handleOpen = this.handleOpen.bind(this);
        this.hendleShow = this.hendleShow.bind(this);
        this.stopButtonsPropagation = this.stopButtonsPropagation.bind(this);
        this.itemId = this.itemId;
        this.items = this.items;
        this.state = {
            itemName: '',
            itemTemp: '',
        };
    }

    stopButtonsPropagation(event) {
        event.stopPropagation();
    }

    hendleShow(name, temp, item) {
        this.itemId = item;
        this.setState({
            itemName: name,
            itemTemp: temp,
        });
        this.props.dispatch({
            type: 'RENAME_CITY',
            open: [...this.props.show, { open: true }],
        });
    }

    handleOpen(item, itemStatus) {
        this.items = itemStatus;
        this.itemId = item;
        this.props.dispatch({ type: 'DELET_REMOVE_MODAL', show: [...this.props.show, { show: true }] });
    }

    upOrDown(city, i, up_Down) {
        const temp = (up_Down == 'UP') ? this.props.cities[i - 1] : this.props.cities[i + 1];
        if (temp == this.props.cities[i - 1]) {
            [this.props.cities[i], this.props.cities[i - 1]] = [this.props.cities[i - 1], this.props.cities[i]]
            this.props.cities[i] = temp;
        } else {
            [this.props.cities[i], this.props.cities[i + 1]] = [this.props.cities[i + 1], this.props.cities[i]]
            this.props.cities[i] = temp
        }

        this.props.dispatch({ type: 'CITY_DATA_UP_OR_DOWN', payload: [...this.props.cities] })
    }


    render() {
        return (
            <>
                {
                    this.props.show.map(item => (
                        <>
                            <RenameModal show={item.open} item={this.itemId} name={this.state.itemName} temp={this.state.itemTemp} />
                            <BootstrapModal show={item.show} item={this.itemId} elemStatus={this.items} />
                        </>
                    ))

                }
                {

                    this.props.cities.map((item, i) => (

                        <tr key={item.id} onClick={() => this.hendleShow(item.name, item.temp, item.id)} >
                            <td className="tdNumber" >{i + 1}.</td>
                            <td className="tdName" >{item.name}</td>
                            <td className="tdTemp" >{item.temp}<sup>o</sup>C</td>
                            <td className='tdButton' onClick={this.stopButtonsPropagation}>
                                <Button name='UP' disabled={(i == 0) ? true : false} onClick={() => this.upOrDown(item, i, 'UP')} />
                                <Button name='DOWN' disabled={((i + 1) == this.props.cities.length) ? true : false} onClick={() => this.upOrDown(item, i, 'DOWN')} />
                                <Button name={(item.status == 'Active') ? 'DELETED' : 'REMOVE'} bsStyle="primary" onClick={() => this.handleOpen(item.id, item.status)} />
                            </td>
                        </tr>
                    ))
                }
            </>

        )
    }
}


const mapStateToProps = (state) => ({
    show: state.modalReducer
});



export default connect(mapStateToProps)(CityData);
