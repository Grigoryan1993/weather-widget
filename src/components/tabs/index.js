import React from 'react';
import { Link } from 'react-router-dom';

class Tabs extends React.Component {
    render() {
        return (
            <div className='statusBar'>
                <div name='All' className='all'><Link to='/all' >All</Link></div>
                <div name='Active' className='active'><Link to='/active' >Active</Link></div>
                <div name='Deleted' className='deleted'><Link to='/deleted' >Deleted</Link></div>
            </div>
        )
    }
}

export default Tabs;