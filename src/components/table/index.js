import React from 'react';
import CityData from '../tables/cityData';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';


class Table extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            filteredCities: this.props.cities
        }
    }


    componentDidUpdate(prevProps) {
        if (this.props.location !== prevProps.location) {
            this.onRouteChanged();
            this.filterCities();
        }
        if (this.props.cities !== prevProps.cities) {
            this.filterCities();
        }
    }

    onRouteChanged() {
        this.filterCities();
    }

    filterCities() {
        let filteredCities;
        switch (this.props.location.pathname) {
            case '/active':
                filteredCities = this.props.cities.filter(item => item.status === 'Active')
                break;

            case '/deleted':
                filteredCities = this.props.cities.filter(item => item.status === 'Deleted')
                break;

            default:
                filteredCities = this.props.cities
                break;
        }
        this.setState({ filteredCities });
    }

    render() {

        return (
            <table >
                <thead>
                    <tr>
                        <th>City</th>
                        <th>Temperature</th>
                        <th>Management</th>
                    </tr>
                </thead>
                <tbody>
                    <CityData cities={this.state.filteredCities} />
                </tbody>
            </table>
        )
    }
}

const mapStateToProps = (state) => ({
    cities: state.getReducer
})

export default withRouter(connect(mapStateToProps)(Table));