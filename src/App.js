import React from 'react';
import logo from './logo.svg';
import './App.css';
import Form from './components/search';
import Tabs from './components/tabs';
import Table from './components/table/index';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from './store';

function App() {

  return (
    <Provider store={store}>
      <BrowserRouter>
        <div className='container'>
          <p>Find Out About The Weather Here</p>
          <Form />
          <Tabs />
          <Switch>
            <Route exact path='/'><Table /></Route>
            <Route exact path='/all'><Table /></Route>
            <Route path='/active'><Table /></Route>
            <Route path='/deleted'><Table /></Route>
          </Switch>
        </div>
      </BrowserRouter>
    </Provider>
  )
}

export default App;
